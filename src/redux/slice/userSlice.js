import { createSlice } from "@reduxjs/toolkit";

const initialStare = {
    isLogin:true,
    user: null,
};
const userSlice = createSlice({
    name: "userSlice",
    initialState : initialStare,
    reducers:{
        setUserInfor:(state,action)=>{
            state.user = action.payload;
        }
    },
});
export const {setUserInfor} = userSlice.actions;
export default userSlice.reducer;