import { Tag } from "antd";

export const headColumns = [
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Ten khach hang",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title:"Tai Khoan",
    dataIndex :"taiKhoan",
    key: "taiKhoan",
  },
  {
    title : "Loai Tai Khoan",
    dataIndex : "maLoaiNguoiDung",
    key:"maLoaiNguoiDung",

    render: (text) =>{
        if(text=="QuanTri"){
            return <Tag color="red">Quản Trị </Tag>
        }else{
            return <Tag color="blue">Khách Hàng</Tag>
        }
    },
  },
  {
    title:"Thao Tac",
    dataIndex :"action",
    key: "action",
  },
];
// email : "cuongvo2501@gmail.com"
// hoTen: "Đô La Cường"
// maLoaiNguoiDung: "KhachHang"
// matKhau: "25019"
// soDT: "09562315656"
// taiKhoan: "250196"
