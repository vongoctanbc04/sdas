import { message } from "antd";
import React from "react";
import { userServ } from "../../services/userService";

export default function UserAction({ taiKhoan }) {
  let handleDeleteUser = () => {
    userServ
      .deleteUser(taiKhoan)
      .then((res) => {
        message.success("xoa thanh cong");
        console.log(res);
      })
      .catch((err) => {
        message.error(err.response.data.content);
        console.log(err);
      });
  };
  return (
    <div className="space-x-2">
      <button
        onClick={handleDeleteUser}
        className="px-5 py-2 rounded bg-red-500 text-white"
      >
        XOA
      </button>
      <button className="px-5 py-2 rounded bg-blue-500 text-white">SUA</button>
    </div>
  );
}
